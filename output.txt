Wystawiono dnia 06-11-2017, Wrocław
Sprzedawca:
Magdalena Podgórna ul. Wołowska 12 51-116 Wrocław NIP 9151528755
DEMO FAKTURY Lp. Nazwa towaru lub usługi
PKWiU
1. Usługa stołówkowa
PODSUMOWANIE
Razem: Zapłacono: Pozostało do zapłaty: Słownie: Konto bankowe: Uwagi:
Stawka VAT 8%
Faktura bez podpisu odbiorcy
Faktura DEMO nr 1/11/2017 Data dostawy*: 06-11-2017 Sposób zapłaty: Przelew Termin płatności: 13-11-2017
Nabywca:
Atos Global Delivery Center Polska Spółka Z Ograniczoną Odpowiedzialnością Spółka Komandytowa ul. Królewska 16 00-103 DEMO NIP 5213207288
Ilość 1
Jedn. szt
Cena jedn. brutto
170,00
Wartość brutto 170,00
Stawka VAT
8%
Wartość netto VAT Wartość brutto 157,41 12,59 170,00 157,41 DEMO,59 170,00 170,00 0,00 sto siedemdziesiąt PLN Millennium Bank 91 1160 2202 0000 0002 6138 6327
Osoba upoważniona do wystawienia faktury VAT Magdalena Podgórna
* Data dostawy rozumiana DEMO jako data dokonania lub zakończenia dostawy towarów/wykonania usług
Druk: www.ifirma.pl strona 1 z 1
1/11/2017