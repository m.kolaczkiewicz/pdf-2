package ITextPDF;

import ITextPDF.ITextPdfParser;
import ITextPDF.Invoice;

public class Main {

    public static void main(String[] args) {


        String relativePath = "src/main/resources/faktura_1_11_2017_06-11-2017.pdf";

        Invoice invoice1 = ITextPdfParser.parse(relativePath);
        System.out.println(invoice1.getBuyer());
        System.out.println(invoice1.getSeller());
        System.out.println(invoice1.getInvoiceDate());
    }
}
