package ITextPDF;

import java.util.ArrayList;

public class Invoice {
    private String seller;
    private String buyer;
    private String invoiceDate;
    private ArrayList<String> invoiceItems;

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getInvoiceItem(int itemNunber){
        return invoiceItems.get(itemNunber);
    }

    public ArrayList<String> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(ArrayList<String> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }
}
