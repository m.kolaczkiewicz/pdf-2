package ITextPDF;

import ITextPDF.Invoice;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ITextPdfParser {

    public static Invoice parse(String relativePath) {
        //PdfReader require absolute path to the file
        File file = new File(relativePath);
        String absolutePath = file.getAbsolutePath();


        PdfReader reader = null;
        String parsedText = null;

        try {
            reader = new PdfReader(absolutePath);
            parsedText = PdfTextExtractor.getTextFromPage(reader, 1, new SimpleTextExtractionStrategy());
        } catch (IOException e) {
            System.out.println("Wrong file path");
            e.printStackTrace();
        }



        String seller, buyer, invoiceDate, invoiceItems;

        seller = parsedText.substring(parsedText.lastIndexOf("Nabywca:") + 8, parsedText.indexOf("NIP") + 14);

        buyer = parsedText.substring(parsedText.indexOf("NIP") + 14, parsedText.lastIndexOf("NIP") + 14);

        invoiceDate = parsedText.substring(parsedText.indexOf("Termin płatności:") + 18, parsedText.lastIndexOf("Sprzedawca"));

        invoiceItems = parsedText.substring(parsedText.indexOf("Stawka") + 13, parsedText.lastIndexOf("PODSUMOWANIE"));

        Invoice invoice = new Invoice();
        invoice.setBuyer(buyer);
        invoice.setSeller(seller);


        String[] invoiceItemsArray = invoiceItems.split("\\%");
        invoice.setInvoiceDate(invoiceDate);
        invoice.setInvoiceItems(new ArrayList<String>(Arrays.asList(invoiceItemsArray)));

        reader.close();

        return invoice;
    }
}
