package JPDFText;

import com.qoppa.pdf.TextPosition;
import com.qoppa.pdf.TextSelection;
import com.qoppa.pdfText.PDFText;

import java.io.FileWriter;




public class TestClass {
    public static void main (String [] args)
    {
        String sprzedawca, nabywca, termin, pozycje;
        try
        {
            // Load the document

            PDFText pdfText = new PDFText("src/main/resources/faktura_1_11_2017_06-11-2017.pdf", null);
            // Get the text for the document
            String docText = pdfText.getText();
            // Save the text in a file
            FileWriter output = new FileWriter ("output.txt");
            // splitting string
            sprzedawca = docText.substring(docText.lastIndexOf("Sprzedawca:"), docText.indexOf("NIP") + 14);
            nabywca = docText.substring(docText.lastIndexOf("Nabywca"), docText.lastIndexOf("NIP") + 14);
            termin = docText.substring(docText.indexOf("Termin"), docText.indexOf("Nabywca"));
            //pozycje = docText.substring(docText.indexOf("Stawka") + 13, docText.lastIndexOf("PODSUMOWANIE"));
            System.out.print( sprzedawca +"\n\n"+ nabywca + "\n\n"+termin + "\nPozycje: (ilość/cena jedn./wartość/VAT)\n"  );
            //System.out.println(docText);
            output.write(docText);
            output.close();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
    }
}
