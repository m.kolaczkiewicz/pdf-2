package ApachePDFBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Invoice {
    private String seller, buyer, paymantDate;
    private List<String> postitions;

    public String getSeller() {
        return seller;
    }

    public String getBuyer() {
        return buyer;
    }

    public String getPaymantDate() {
        return paymantDate;
    }

    public List<String> getPostitions() {
        return postitions;
    }

    public String getPositions(int i) {
        return postitions.get(i);
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public void setPaymantDate(String paymantDate) {
        this.paymantDate = paymantDate;
    }

    public void setPostition(String position) {
        this.postitions.add(position);
    }

    public void setPositions(List<String> positions) {
        this.postitions = positions;
    }
}
