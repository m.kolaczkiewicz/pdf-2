package ApachePDFBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class PDFParser {
    public static Invoice parse(String filePath) throws IOException {
        File file = new File(filePath);
        PDDocument pdDoc = PDDocument.load(file);
        PDFTextStripper pdfStripper = new PDFTextStripper();
        String text = pdfStripper.getText(pdDoc);
        Invoice invoice = new Invoice();
        invoice.setSeller(text.substring(text.lastIndexOf("Nabywca:") + 8, text.indexOf("NIP") + 14));
        invoice.setBuyer(text.substring(text.indexOf("NIP") + 14, text.lastIndexOf("NIP") + 14));
        invoice.setPaymantDate(text.substring(text.indexOf("Termin płatności:") + 18, text.lastIndexOf("Sprzedawca")));
        String pos = text.substring(text.indexOf("Stawka") + 13, text.lastIndexOf("PODSUMOWANIE"));
        invoice.setPositions(new ArrayList(Arrays.asList(pos.split("\\s*%\\s*"))));
        return invoice;
    }
}
