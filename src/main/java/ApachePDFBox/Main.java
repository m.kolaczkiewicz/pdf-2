package ApachePDFBox;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Invoice invoice = PDFParser.parse("src/main/resources/faktura_1_11_2017_06-11-2017.pdf");
        System.out.println("Sprzedawca:\n"+invoice.getSeller());
        System.out.println("Nabywca:\n"+invoice.getBuyer());
        System.out.println("Termin zapłaty:\n"+invoice.getPaymantDate());
        System.out.println("Pozycje: (ilość/cena jend./wartość/VAT)\n "+invoice.getPositions(0));
    }
}
